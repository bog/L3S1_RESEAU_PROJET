#include <mylib.h>

void strstdout(char* str)
{
  assert(str);
  if( write(1, str, strlen(str)) == -1 )
    {
      fatal_error("Impossible d'ecrire sur stdout");
    }
}

void strstderr(char* str)
{
  assert(str);
  COLOR_KO;
  write(2, "E : ", 4);
  write(2, str, strlen(str));
  write(2, " !\n", 3);
  COLOR_END;
  
  perror("System status ");
}

void stdinstr(char* str, size_t *size)
{
  assert(str);
  
  char c;
  size_t s = 0;
  
  while(read(0, &c, sizeof(char)) && c != '\n')
    {
      str[s] = c;
      s++;
    }

  str[s] = '\0';
  
  if(size != NULL)
    {
      *size = s;
    }
}

void fatal_error(char* str)
{
  strstderr(str);
  exit(EXIT_FAILURE);
}

int is_file_exists(char* str)
{
  assert(str);

  int file_descriptor = 1;

  file_descriptor = open(str, O_RDONLY);

  int exists = (file_descriptor != -1);

  close(file_descriptor);
  
  return exists;
}


int is_port_valid(int port)
{
  return (port >= 1024);
}
