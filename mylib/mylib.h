#ifndef MY_LIB_H
#define MY_LIB_H
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

#define STR_LIMIT_SIZE 256

#define COLOR_OK strstdout("\e[32m")
#define COLOR_KO strstdout("\e[31m")
#define COLOR_END strstdout("\e[0m") 

enum
  {
    E_MSG_FILE_ALREADY_EXISTS_ERROR,
    E_MSG_ACK,
    E_MSG_END
  };

typedef struct
{
  char content;
  int flag;
} message_t;

// read a string until \n in stdin and put it in str
// and update size according to the size of what have been given. 
void stdinstr(char* str, size_t *size);

// write a string str in stdout
void strstdout(char* str);

// write a string str in stderr
void strstderr(char* str);

// write an error message and quit
void fatal_error(char* str);

// return 1 if the file exists, 0 else
int is_file_exists(char* str);

// return 1 if the port number is valid
int is_port_valid(int port);

#endif // MY_LIB_H
