#ifndef SERVEUR_UTILS_H
#define SERVEUR_UTILS_H
#include <mylib.h>

typedef struct 
{
  unsigned int port;
  int socket;
  struct sockaddr_in addr;
  socklen_t addr_sz;
  unsigned int max_client;
  
} server_t;

void server_init(server_t *self, unsigned int port, unsigned int max_client);
int server_get_client(server_t* self);
void server_bind(server_t* self);
void server_destroy(server_t *self);

#endif //SERVEUR_UTILS_H
