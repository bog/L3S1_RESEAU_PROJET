#include <serveur_utils.h>

void server_init(server_t *self, unsigned int port, unsigned int max_client)
{
  assert(self);
  
  // init socket
  self->socket = socket(AF_INET, SOCK_STREAM, 0);

  if( self->socket == -1 )
    {
      fatal_error("Erreur socket");
    }

  self->addr.sin_family = AF_INET;
  self->addr.sin_port = htons(port);
  self->addr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  self->addr_sz = sizeof(self->addr);
  self->max_client = max_client;

}

void server_bind(server_t* self)
{
  assert(self);

  int bind_return;

  bind_return = bind(self->socket, (struct sockaddr*)(&self->addr), self->addr_sz);

  if(bind_return == -1)
    {
      fatal_error("Erreur bind");
    }

  int listen_return;

  listen_return = listen(self->socket, self->max_client);

  if(listen_return == -1)
    {
      fatal_error("Erreur listen");
    }
}

int server_get_client(server_t* self)
{
  assert(self);

  int client_socket = accept(self->socket, NULL, NULL);

  if( client_socket == -1 ) { fatal_error("Erreur socket client"); }

  return client_socket;
}

void server_destroy(server_t *self)
{
  assert(self);
  
  // close socket
  close(self->socket);
}

