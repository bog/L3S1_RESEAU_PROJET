#include <stdio.h>
#include <stdlib.h>
#include <serveur_utils.h>

int main(int argc, char** argv)
{
  unsigned int port;

  // get informations
  // from stdin
  if( argc == 1 )
    {
      // obtenir le numero de port
      strstdout("Veuillez saisir le numero de port : ");
      char tmp[STR_LIMIT_SIZE];
      stdinstr(tmp, NULL);
      port = atoi(tmp);
    }
  // from args
  else if( argc == 2 )
    {
      port = atoi(argv[1]);
    }
  else
    {
      fatal_error("Usage : serveur <port>");
    }
    
  if( !is_port_valid(port) )
    {
      fatal_error("Numero de port invalide");
    }

  server_t server;
  server_init(&server, port, 20);
  server_bind(&server);

  COLOR_OK;
  strstdout("Serveur pret\n");
  COLOR_END;

  int socket_client = 0;

  while( (socket_client=server_get_client(&server)) != -1 )
    {
      // réception du nom du fichier
      char file_name[STR_LIMIT_SIZE];
      read(socket_client, &file_name, sizeof(file_name));
      
      message_t msg;

      // vérification de la non existance du fichier
      // et envoi de l'etat du serveur
      // (ERROR ou ACK)
      if( is_file_exists(file_name) )
	{
	  msg.flag = E_MSG_FILE_ALREADY_EXISTS_ERROR;
	}
      else
	{
	  msg.flag = E_MSG_ACK;
	}

      write(socket_client, &msg, sizeof(msg));

      int file = creat(file_name, S_IRWXU | S_IRWXO);
      if(file == -1){ fatal_error("Impossible de créer le fichier"); }
      
      // reception du contenu
      size_t already_reiceved = 0;

      while( read(socket_client, &msg, sizeof(msg)) != 0 
	     && msg.flag != E_MSG_END )
      	{
      	  write(file, &(msg.content), sizeof(msg.content));
	  already_reiceved += sizeof(msg.content);
      	}
      
      {
	char final_msg[STR_LIMIT_SIZE];
	sprintf(final_msg, "Reception de %s -- %zd octet(s)\n"
		, file_name
		, already_reiceved);

	if( already_reiceved == 0){ COLOR_KO; }
	else { COLOR_OK; }

	strstdout(final_msg);
	COLOR_END;
      }

      // envoyer la taille lu
      write(socket_client, &already_reiceved, sizeof(already_reiceved));

      close(file);
      close(socket_client);
    }
  
  server_destroy(&server);
  
  return 0;
}
