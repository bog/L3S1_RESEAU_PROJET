#include <stdio.h>
#include <stdlib.h>
#include <client_utils.h>

int main(int argc, char** argv)
{
  char server_name[STR_LIMIT_SIZE];
  char port_name[STR_LIMIT_SIZE];
  char file_name[STR_LIMIT_SIZE];
  size_t file_size;
  char new_file_name[STR_LIMIT_SIZE];
  unsigned int port;
  
  // get information
  // from stdin
  if(argc == 1)
    {
      strstdout("Veuillez saisir le nom du serveur : ");
      stdinstr((char*)(server_name), NULL);

      strstdout("Veuillez saisir le numéro de port : ");
      stdinstr((char*)(port_name), NULL);
      port = atoi(port_name);

      strstdout("Veuillez saisir le nom du fichier vidéo à envoyer : ");
      stdinstr((char*)(file_name), &file_size);

      strstdout("Veuillez saisir le nom du fichier vidéo sur le serveur : ");
      stdinstr((char*)(new_file_name), NULL);
    }
  else if(argc == 5)
    {
      strcpy(server_name, argv[1]);
      strcpy(port_name, argv[2]);
      port = atoi(port_name);
      strcpy(file_name, argv[3]);
      file_size = strlen(file_name);
      strcpy(new_file_name, argv[4]);
    }
  else
    {
      fatal_error("Usage : client <serveur> <port> <fichier> <nom>");
    }

  if( file_size == 0 )
    {
      fatal_error("Nom vide non autorisé");
    }

  if( !is_file_exists(file_name) )
    {
      fatal_error("Le fichier n'existe pas !");
    }
  
  if( !is_port_valid(port) )
    {
      fatal_error("Numero de port invalide");
    }
  
  client_t client;
  client_init(&client);
  
  int connect_return;
  connect_return = client_connect(&client, server_name, port);

  if( connect_return == -1 )
    {
      fatal_error("Connexion au serveur impossible");
    }  
  
  // envoi du nom du fichier
  write(client.socket, new_file_name, sizeof(new_file_name));

  message_t msg;
  // reception de l'ack
  read(client.socket, &msg, sizeof(msg));

  if(msg.flag == E_MSG_FILE_ALREADY_EXISTS_ERROR)
    {
      fatal_error("Le fichier existe déjà sur le serveur");
    }

  int file = open(file_name, O_RDONLY);
  if(file == -1){ fatal_error("Impossible d'ouvrir le fichier"); }  
  
  size_t already_send = 0;
  // envoi du contenu du fichier
  while(read(file, &(msg.content), sizeof(msg.content)) != 0)
    {
      write(client.socket, &msg, sizeof(msg));
      already_send++;
    }

  // end communication
  msg.flag = E_MSG_END;
  write(client.socket, &msg, sizeof(msg));
  
  // reception taille lu
  size_t already_reiceved = 0;
  read(client.socket, &already_reiceved, sizeof(already_reiceved));
  
  {
    char info_msg[STR_LIMIT_SIZE];
    sprintf(info_msg, "%zd octet(s) recu par le serveur\n", already_reiceved);
    COLOR_OK;
    strstdout(info_msg);
    COLOR_END;
  }
  
  if(already_reiceved == already_send)
    {
      COLOR_OK;
      strstdout("Le fichier a bien été envoyé.\n");
      COLOR_END;
    }
  else
    {
      COLOR_KO;
      strstdout("Le fichier est corrompu.\n");
      COLOR_END;
    }

  close(file);
  client_destroy(&client);
  
  return 0;
}
