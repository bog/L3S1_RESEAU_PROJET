#ifndef CLIENT_UTILS_H
#define CLIENT_UTILS_H
#include <mylib.h>
#include <netdb.h>

typedef struct
{
  int socket;
  struct addrinfo* server_info;
  struct addrinfo* server;
} client_t;


void client_init(client_t* self);
void client_destroy(client_t* self);
int client_connect(client_t* self, char const* server_name, unsigned int port);

#endif // CLIENT_UTILS_H
