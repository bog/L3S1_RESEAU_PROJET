#include <client_utils.h>

void client_init(client_t* self)
{
  assert(self);
  self->server_info = NULL;
  self->server = NULL;
  self->socket = -1;
}

void client_destroy(client_t* self)
{
  assert(self);

  if( self->server_info != NULL )
    {
      freeaddrinfo(self->server_info);
      self->server_info = NULL;
      self->server = NULL;
    }

  if( self->socket != -1 )
    {
      close(self->socket);
    }
}

int client_connect(client_t* self, char const* server_name, unsigned int port)
{
  assert(self);
  assert(self->server_info == NULL);

  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_CANONNAME;
  
  char port_str[STR_LIMIT_SIZE];
  sprintf(port_str, "%d", port);
  
  int gai_return = getaddrinfo(server_name, port_str, &hints, &(self->server_info));
  if( gai_return != 0 )
    {
      char error_msg[STR_LIMIT_SIZE];
      sprintf(error_msg, "Erreur lors de la connexion, %s", gai_strerror(gai_return));
      fatal_error(error_msg);
    }

  for(self->server = self->server_info; self->server != NULL; self->server = self->server->ai_next)
    {
      self->socket = socket(self->server->ai_family
  			    , self->server->ai_socktype
  			    , self->server->ai_protocol);
      
      strstdout("Tentative de connexion au serveur... ");
      
      if( connect(self->socket, self->server->ai_addr, self->server->ai_addrlen) == -1 )
  	{
  	  COLOR_KO;
  	  strstdout("échouée.\n");
  	  COLOR_END;
  	  close(self->socket);
  	  self->socket = -1;
  	  continue;
  	}
      
      COLOR_OK;
      strstdout("réussie.\n");
      COLOR_END;
      break;
    }

  if(self->server == NULL)
    {
      return -1;
    }
}

