##### Mini Projet : Écriture d'un logiciel de transfert de fichier #####

# 1 - Compilation du projet
Exécutez les commandes suivantes à la racine du projet.
$ mkdir build
$ cd build
$ cmake ../
$ make

# 2 - Utilisation
$ client <serveur> <port> <fichier> <nom>
ou
$ client

$ serveur <port> 
ou
$ serveur
